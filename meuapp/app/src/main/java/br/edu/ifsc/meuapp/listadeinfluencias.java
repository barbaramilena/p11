package br.edu.ifsc.meuapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class listadeinfluencias extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listadeinfluencias);

        ListView lista = (ListView) findViewById(R.id.moda);

       ArrayList<String> equipes = preencherDados();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, equipes);
        lista.setAdapter(arrayAdapter);


    }

    private ArrayList<String> preencherDados() {
        ArrayList<String> dados = new ArrayList<String>();
        dados.add("Aimee Song");
        dados.add("Julia Engel");
        dados.add("Thássia Naves");
        dados.add("Chiara Ferragni");
        dados.add("Camila Coelho");
        return dados;

    }


}

