package br.edu.ifsc.meuapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button pg = (Button) findViewById(R.id.roupas);
        pg.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent bas = new Intent(MainActivity.this, moda.class);
                startActivity(bas);
            }
        });
    }

}
