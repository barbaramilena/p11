package br.edu.ifsc.meuapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class moda extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moda);
        Button list = (Button) findViewById(R.id.list);
        list.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                Intent bas = new Intent(moda.this, listadeinfluencias.class);
                startActivity(bas);
            }
        });
    }
}
